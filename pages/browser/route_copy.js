import React, { useState, useEffect } from "react";

import { useRouter } from "next/router";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Button from "@material-ui/core/Button";
import axios, { post } from "axios";
import { ThemeProvider } from "@material-ui/styles";
import theme from "../../components/theme.js";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Cookies from "cookies";
import Dashboard_page from "./dashboard_page";
import Manageroom_page from "./manageroom_page";
import Manageremember_page from "./manageremember_page";
import Checkslip_page from "./checkslip_page";
import Cancel_page from "./cancel_page";
import History_page from "./history_page";
import Withdraw_page from "./withdraw_page";
const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));
const Header = () => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const [register_count, Register_count] = useState("0");
  const [register_empty, Register_empty] = useState(true);
  const [bill_count, Bill_count] = useState("0");
  const [bill_empty, Bill_empty] = useState(true);
  const [withdraw_count, Withdraw_count] = useState("0");
  const [withdraw_empty, Withdraw_empty] = useState(true);
  const [cancel_count, Cancel_count] = useState("0");
  const [cancel_empty, Cancel_empty] = useState(true);
  const [savingroom_count, Savingroom_count] = useState("0");
  const [savingroom_empty, Savingroom_empty] = useState(true);
  useEffect(() => {
    axios.get("/api/check_status").then(function (response) {
      // console.log(response);
      if (response.data.data.Register_ > 0) {
        Register_count(response.data.data.Register_);
        Register_empty(false);
      } else {
        Register_count("0");
        Register_empty(true);
      }
      if (response.data.data.Bill_ > 0) {
        Bill_count(response.data.data.Bill_);
        Bill_empty(false);
      } else {
        Bill_count("0");
        Bill_empty(true);
      }
      if (response.data.data.Withdraw_ > 0) {
        Withdraw_count(response.data.data.Withdraw_);
        Withdraw_empty(false);
      } else {
        Withdraw_count("0");
        Withdraw_empty(true);
      }
      if (response.data.data.Cancel_ > 0) {
        Cancel_count(response.data.data.Cancel_);
        Cancel_empty(false);
      } else {
        Cancel_count("0");
        Cancel_empty(true);
      }
      if (response.data.data.SavingRooms_ > 0) {
        Savingroom_count(response.data.data.SavingRooms_);
        Savingroom_empty(false);
      } else {
        Savingroom_count("0");
        Savingroom_empty(true);
      }
    });
  }, []);

  setInterval(() => {
    axios.get("/api/check_status").then(function (response) {
      // console.log(response);
      if (response.data.data.Register_ > 0) {
        Register_count(response.data.data.Register_);
        Register_empty(false);
      } else {
        Register_count("0");
        Register_empty(true);
      }
      if (response.data.data.Bill_ > 0) {
        Bill_count(response.data.data.Bill_);
        Bill_empty(false);
      } else {
        Bill_count("0");
        Bill_empty(true);
      }
      if (response.data.data.Withdraw_ > 0) {
        Withdraw_count(response.data.data.Withdraw_);
        Withdraw_empty(false);
      } else {
        Withdraw_count("0");
        Withdraw_empty(true);
      }
      if (response.data.data.Cancel_ > 0) {
        Cancel_count(response.data.data.Cancel_);
        Cancel_empty(false);
      } else {
        Cancel_count("0");
        Cancel_empty(true);
      }
      if (response.data.data.SavingRooms_ > 0) {
        Savingroom_count(response.data.data.SavingRooms_);
        Savingroom_empty(false);
      } else {
        Savingroom_count("0");
        Savingroom_empty(true);
      }
    });
  }, 10000);

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const router = useRouter();
  const OnLogout = async (e) => {
    e.preventDefault();
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      url: "/api/logout",
    }).then(
      (response) => {
        return router.push("/browser/login");
      },
      (error) => {
        console.log(error);
      }
    );
  };

  return (
    <>
      <Router>
        <ThemeProvider theme={theme}>
          <div className={classes.root}>
            <CssBaseline />
            <AppBar
              position="fixed"
              className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
              })}
            >
              <Toolbar className="d-flex bd-highlight mb-3">
                <IconButton
                  className="p-2 bd-highlight"
                  color="inherit"
                  aria-label="open drawer"
                  onClick={handleDrawerOpen}
                  edge="start"
                  className={clsx(classes.menuButton, {
                    [classes.hide]: open,
                  })}
                >
                  <MenuIcon />
                </IconButton>
                <Typography className="p-2 bd-highlight" variant="h6" noWrap>
                  ร้านทองอารมณ์ดี (แอดมิน)
                </Typography>
                <div className="ml-auto p-2 bd-highlight">
                  <Button
                    onClick={OnLogout}
                    variant="contained"
                    color="default"
                    className={classes.button}
                    endIcon={<ExitToAppIcon />}
                  >
                    ออกจากระบบ
                  </Button>
                </div>
              </Toolbar>
            </AppBar>
            <Drawer
              variant="permanent"
              className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
              })}
              classes={{
                paper: clsx({
                  [classes.drawerOpen]: open,
                  [classes.drawerClose]: !open,
                }),
              }}
            >
              <div className={classes.toolbar}>
                <IconButton onClick={handleDrawerClose}>
                  {theme.direction === "rtl" ? (
                    <ChevronRightIcon />
                  ) : (
                    <ChevronLeftIcon />
                  )}
                </IconButton>
              </div>
              <Divider />
              <List>
                <Link to="/">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/แดชบอร์ด.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                    </ListItemIcon>
                    <ListItemText primary={"แดชบอร์ด"} />
                  </ListItem>
                </Link>
                <Link to="/manageroom">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/จัดการห้องออม.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                      <div
                        hidden={savingroom_empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {savingroom_count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"จัดการห้องออม"} />
                  </ListItem>
                </Link>
                <Link to="/manageremember">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/จัดการสมาชิก.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                      <div
                        hidden={register_empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {register_count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"จัดการสมาชิก"} />
                  </ListItem>
                </Link>
                <Link to="/cancel">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/ยกเลิกการออม.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                      <div
                        hidden={cancel_empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {cancel_count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"ยกเลิกออม"} />
                  </ListItem>
                </Link>
                <Link to="/withdraw">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/รายการถอน.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                      <div
                        hidden={withdraw_empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {withdraw_count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"รายการถอน"} />
                  </ListItem>
                </Link>
                <Link to="/checkslip">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/ตรวจสอบสลิป.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                      <div
                        hidden={bill_empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {bill_count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"ตรวจสอบสลิป"} />
                  </ListItem>
                </Link>
                <Link to="/history">
                  <ListItem>
                    <ListItemIcon>
                      <img
                        src="/image/icon_browser/ข้อมูลย้อนหลัง.png"
                        className="d-flex justify-content-center"
                        width={"24px"}
                        height={"24px"}
                      />
                    </ListItemIcon>
                    <ListItemText primary={"ข้อมูลย้อนหลัง"} />
                  </ListItem>
                </Link>
              </List>
            </Drawer>
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <Typography>
                <Switch>
                  <Route path="/withdraw">
                    <Withdraw_page />
                  </Route>
                  <Route path="/history">
                    <History_page />
                  </Route>
                  <Route path="/cancel">
                    <Cancel_page />
                  </Route>
                  <Route path="/checkslip">
                    <Checkslip_page />
                  </Route>
                  <Route path="/manageremember">
                    <Manageremember_page />
                  </Route>
                  <Route path="/manageroom">
                    <Manageroom_page />
                  </Route>
                  <Route path="/">
                    <Dashboard_page />
                  </Route>
                </Switch>
              </Typography>
            </main>
          </div>
        </ThemeProvider>
      </Router>
    </>
  );
};
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  // console.log(user);
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};
export default Header;
