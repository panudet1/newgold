import Cors from "cors";
import initMiddleware from "../../lib/init-middleware";
import axios from "axios";
const cors = initMiddleware(
  Cors({
    methods: ["GET", "POST", "OPTIONS"],
  })
);

export default async function handler(req, res) {
  await cors(req, res);
  if (req.method === "POST") {
    var id = req.body.id;
    const response = await axios.get(
      "https://api.line.me/v2/bot/profile/" + id,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer " +
            "bGwVCUdjL+uTBuEeVR4qfyqSuO/Gi2yFg8KrAiuQGxpJJ3HA5fD6S8XYcsZ89NDvCy2BpjxDeB48tZGrJcJzs1Un8e1OHBDOjnsF77iYU8meIFhdAf9yChgav8kQkMrg3p3+4cK+xKjJIdCjxoZzxAdB04t89/1O/w1cDnyilFU=",
        },
      }
    );
    var data = response.data;
    return res.status(200).json({ data });
  }
}
