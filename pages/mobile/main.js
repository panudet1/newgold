import React, { useState, useEffect } from "react";
import axios, { post } from "axios";
import { useRouter } from "next/router";
import Refresh from "@material-ui/icons/Refresh";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ModalReact from "react-modal";
const customStyles2 = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

export default function LabelBottomNavigation() {
  const router = useRouter();
  const [goldprice, setGoldprice] = useState(Array);
  const [isloading, Isloading] = useState(false);
  const [total_owner, Total_owner] = useState(Number);
  const [total_user, Total_user] = useState(Number);
  const [us_id, Us_is] = useState(localStorage.getItem("auth"));
  // const [us_id, Us_is] = useState("Ue217b0b4a8667c0f09e731ee9e4af0b0");
  // const [us_id, Us_is] = useState("Ue217b0b4a8667c0f09e731ee9e4af0b0");
  // const [us_id, Us_is] = useState("Uf08604dbfae9ffdc8123032a8cb2d26c3"); //test 1
  // const [us_id, Us_is] = useState("Uf08604dbfae9ffdc8123032a8cb2d26c"); //test Robert
  useEffect(async () => {
    GoldPrice();
    LoadAllRoom();
    await localStorage.setItem("auth", us_id);
  }, []);
  const RefreshData = async () => {
    GoldPrice();
  };
  const GoldPrice = async () => {
    await axios({
      method: "get",
      url: "https://thai-gold-api.herokuapp.com/latest",
    }).then((response) => {
      setGoldprice(response.data.response.price.gold_bar);
    });
  };
  const LoadAllRoom = async () => {
    var data = {
      user_id: us_id,
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/check_all_room_by_user_id",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        var owner = 0;
        var user = 0;

        for (let index = 0; index < response.data.data.length; index++) {
          const element = response.data.data[index];
          if (
            Date.parse(element.savingrooms_data[0].end_date) <
            Date.parse(new Date())
          ) {
          } else {
            if (element.savingrooms_data[0].status === "finished") {
            } else {
              if (element.owner === us_id) {
                owner = owner + 1;
              } else {
                user = user + 1;
              }
            }
          }
        }

        Total_owner(owner);
        Total_user(user);
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const go_to_list_room = async () => {
    return router.push("/mobile/list_room");
  };
  return (
    <>
      <div
        style={{
          height: "90vh",
          backgroundColor: "rgb(156, 0, 0)",
          overflow: "scroll",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-3 d-flex justify-content-start">
              {/* <Refresh type="submit" style={{ fontSize: 40, color: "#fff" }} /> */}
            </div>
            <div
              className="col-6 d-flex justify-content-center"
              style={{
                fontFamily: "SukhumvitSet-SemiBold",
                color: "rgb(0,190,22)",
                fontSize: "3vw",
              }}
            >
              <img
                src="/image/logo.svg"
                className="d-flex justify-content-center mt-5"
                width={"300px"}
                height={"150px"}
              />
            </div>
            <div className="col-3 d-flex justify-content-end pt-3">
              {/* <Refresh type="submit" style={{ fontSize: 40, color: "#fff" }} /> */}
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <div className="card" style={{ width: "90vw" }}>
            <div className="card-body">
              <div className="container">
                <div className="row">
                  <div
                    className="col-4"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(153,0,0)",
                      fontSize: "3vw",
                    }}
                  >
                    ราคาทองสมาคม
                  </div>
                  <div
                    className="col-4"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    ซื้อเข้า
                  </div>
                  <div
                    className="col-4"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    ขายออก
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-4"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(231,180,0)",
                      fontSize: "3vw",
                    }}
                  >
                    96.5%
                  </div>
                  <div
                    className="col-4"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    {goldprice.sell}
                  </div>
                  <div
                    className="col-4"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    {goldprice.buy}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center mt-2  mb-5">
          <div
            className="card"
            style={{
              width: "90vw",
              backgroundImage: "linear-gradient(rgb(101,0,0), rgb(53,0,0))",
              color: "#fff",
              fontSize: "10pt",
            }}
            onClick={go_to_list_room}
          >
            <div className="card-body">
              <div className="row">
                <div className="col-10">
                  <div className="form-row">
                    <div className="col">
                      <p className="card-text">
                        เป็นเจ้าของ : {total_owner} ห้อง
                      </p>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="col">
                      <p className="card-text">
                        เป็นสมาชิก : {total_user} ห้อง
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-2">
                  <ArrowForwardIosIcon
                    style={{
                      fontSize: 30,
                      padding: "3px",
                      color: "#fff",
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ModalReact style={customStyles2} isOpen={isloading}>
        <button className="btn btn-primary" type="button" disabled>
          <span
            className="spinner-grow spinner-grow-sm"
            role="status"
            aria-hidden="true"
          />
          กรุณารอสักครู่...
        </button>
      </ModalReact>
    </>
  );
}
