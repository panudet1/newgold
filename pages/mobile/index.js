import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios, { post } from "axios";
import {
  isChrome,
  isFirefox,
  isSafari,
  isOpera,
  isIE,
  isEdge,
  isYandex,
  isChromium,
  isMobile,
} from "react-device-detect";
import Swal from "sweetalert2";
const Page = () => {
  const router = useRouter();
  const [line_id, Line_id] = useState(String);

  try {
    useEffect(async () => {
      await liff.init({ liffId: `1655384823-M5awVAzb` }).catch((err) => {
        throw err;
      });

      // console.log(liff.isLoggedIn());
      if (liff.isLoggedIn()) {
        let getProfile = await liff.getProfile();
        const id = await getProfile.userId;

        // console.log("log หน้านี้" + id);

        if (isMobile) {
          await localStorage.setItem("auth", id);
          // Line_id(await localStorage.getItem("auth"));
          await axios({
            method: "post",
            headers: {
              "Content-Type": "application/json",
            },
            url: "/api/index_mobile",
            data: JSON.stringify({ id }),
          }).then(
            (response) => {
              if (response.status === 200) {
                return router.push("/mobile/login");
              }
              if (response.status === 203) {
                return router.push("/mobile/confirm_password");
              }
              if (response.status === 201) {
                return router.push("/mobile/wait");
              }
              if (response.status === 202) {
                return router.push("/mobile/register");
              }
            },
            (error) => {
              console.log(error);
            }
          );
          // return <></>;
        } else {
          await localStorage.setItem("auth_backend", id);
          const user_id = id;
          await axios({
            method: "post",
            headers: {
              "Content-Type": "application/json",
            },
            url: "/api/login_admin",
            data: JSON.stringify({ user_id }),
          }).then(
            (response) => {
              if (response.status === 200) {
                var browser_name = "";
                if (isChrome) {
                  browser_name = "isChrome";
                }
                if (isFirefox) {
                  browser_name = "isFirefox";
                }
                if (isSafari) {
                  browser_name = "isSafari";
                }
                if (isOpera) {
                  browser_name = "isOpera";
                }
                if (isIE) {
                  browser_name = "isIE";
                }
                if (isEdge) {
                  browser_name = "isEdge";
                }
                if (isYandex) {
                  browser_name = "isYandex";
                }
                if (isChromium) {
                  browser_name = "isChromium";
                }
                var data = {
                  user_id: response.data.data,
                  device: "",
                  browser_name: browser_name,
                };
                axios({
                  method: "post",
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer my-token",
                    "My-Custom-Header": "foobar",
                  },
                  url: "/api/login_history",
                  data: JSON.stringify({ data: data }),
                }).then(
                  (response) => {
                    return router.push("/browser/route");
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              }
              if (response.status === 201) {
                Swal.fire({
                  icon: "error",
                  title: "Oops...",
                  text: "ไม่พบบัญชีผู้ใช้!",
                });
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      } else {
        liff.login();
      }
    }, []);
  } catch (error) {}

  return <div></div>;
};
export default Page;
