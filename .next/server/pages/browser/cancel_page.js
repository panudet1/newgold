module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 89);
/******/ })
/************************************************************************/
/******/ ({

/***/ "4jCe":
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Visibility");

/***/ }),

/***/ "5Buo":
/***/ (function(module, exports) {



/***/ }),

/***/ 89:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Euce");


/***/ }),

/***/ "CBiN":
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ }),

/***/ "Euce":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getServerSideProps", function() { return getServerSideProps; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("F5FC");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var cookies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("Vjj2");
/* harmony import */ var cookies__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(cookies__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mdbreact__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("Y52P");
/* harmony import */ var mdbreact__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mdbreact__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("UVoM");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("zr5I");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("wy2R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("SibU");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("qqGZ");
/* harmony import */ var react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("CBiN");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("ZTWx");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_datepicker_dist_react_datepicker_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("5Buo");
/* harmony import */ var react_datepicker_dist_react_datepicker_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_datepicker_dist_react_datepicker_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react_number_format__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__("uM63");
/* harmony import */ var react_number_format__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_number_format__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__("4jCe");
/* harmony import */ var _material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_13__);

















const PromotionPage = ({
  user
}) => {
  const {
    0: seleteddata,
    1: Seleteddata
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(Array);
  const {
    0: datatable,
    1: setDatatable
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(Array);
  const {
    0: flname,
    1: Flname
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String);
  const {
    0: bank_name_admin,
    1: Bank_name_admin
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("กรุณาเลือกธนาคาร...");
  const {
    0: bank_number_admin,
    1: Bank_number_admin
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String);
  const {
    0: bank_account_admin,
    1: Bank_account_admin
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String);
  const {
    0: transfer_datetime,
    1: Transfer_datetime
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(new Date());
  const {
    0: totalvalue,
    1: Totalvalue
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String);
  const {
    0: t_name,
    1: T_name
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String);
  const {
    0: show,
    1: setShow
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const {
    0: us_id,
    1: Us_id
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(localStorage.getItem("auth_backend"));
  const {
    0: us_role,
    1: Us_role
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(localStorage.getItem("role"));
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(async () => {
    LoadDataCancel();
  }, []);

  const OnOpenDetail = async e => {
    if (e.type === "money") {
      Totalvalue(new Intl.NumberFormat().format(e.total));
      T_name("เงิน");
    } else {
      Totalvalue(parseFloat(e.total).toFixed(2) + " กรัม");
      T_name("ทอง");
    }

    Flname(e.users[0].fname + " " + e.users[0].lname);
    Seleteddata(e);
    setShow(true);
  };

  const LoadDataCancel = async () => {
    var dataCencel = await axios__WEBPACK_IMPORTED_MODULE_5___default()({
      method: "get",
      url: "/api/cancel/all_pending_cancel"
    }).then(response => {
      return response.data.data;
    });
    var data = [];
    dataCencel.forEach(element => {
      var total_v = "";

      if (element.type === "money") {
        total_v = new Intl.NumberFormat().format(element.total) + " บาท";
      } else {
        total_v = parseFloat(element.total).toFixed(2) + " กรัม";
      }

      var type_name = "";

      if (element.type === "money") {
        type_name = "เงิน";
      } else {
        type_name = "ทอง";
      }

      var row = {
        user_id: element.users[0].fname + " " + element.users[0].lname,
        type: type_name,
        total: total_v,
        date: moment__WEBPACK_IMPORTED_MODULE_6___default()(element.cancel_date).format("YYYY-MM-DD HH:mm:ss"),
        action: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_13___default.a, {
          onClick: () => OnOpenDetail(element)
        })
      };
      data.push(row);
    });
    var columns = [{
      label: "ชื่อ-นามสกุล",
      field: "user_id",
      width: 150
    }, {
      label: "ประเภท",
      field: "type",
      width: 270
    }, {
      label: "จำนวน",
      field: "total",
      width: 200
    }, {
      label: "วันที่",
      field: "date",
      width: 200
    }, {
      label: "",
      field: "action",
      sort: "disabled",
      width: 100
    }];
    var rows = data;
    var detail = {
      columns,
      rows
    };
    setDatatable(detail);
  };

  const OnCloseDetail = async e => {
    setShow(false);
  };

  const OnDontApprove = async e => {
    // console.log(e);
    var data = {
      id: e._id,
      user_id: e.user_id,
      room_id: e.room_id,
      modify_by: us_id
    };
    axios__WEBPACK_IMPORTED_MODULE_5___default()({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar"
      },
      url: "/api/cancel/decline_cancel",
      data: JSON.stringify({
        data: data
      })
    }).then(response => {
      // console.log(response);
      return sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
        icon: "success",
        title: "เรียบร้อย",
        text: "ปฏิเสธการยกเลิก สำเร็จ"
      });
    }, error => {
      console.log(error);
    });
    var messages = "การยกเลิกออมของท่านได้รับการปฎิเสธ";
    var data = {
      messages: messages,
      user_id: e.user_id
    };
    await axios__WEBPACK_IMPORTED_MODULE_5___default()({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar"
      },
      url: "/api/line_messaging",
      data: JSON.stringify({
        data: data
      })
    }).then(response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
    await LoadDataCancel();
    setShow(false);
  };

  const OnApprove = async e => {
    var b_name = "";

    if (bank_name_admin === "กรุณาเลือกธนาคาร...") {
      b_name = "ไม่ระบุธนาคาร";
    } else {
      b_name = bank_name_admin;
    } // console.log(e);


    var data = {
      id: e._id,
      user_id: e.user_id,
      room_id: e.room_id,
      modify_by: us_id,
      bank_name_admin: b_name,
      bank_number_admin: bank_number_admin,
      bank_account_admin: bank_account_admin,
      transfer_datetime: transfer_datetime
    };
    axios__WEBPACK_IMPORTED_MODULE_5___default()({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar"
      },
      url: "/api/cancel/approve_cancel",
      data: JSON.stringify({
        data: data
      })
    }).then(response => {
      LoadDataCancel();
      setShow(false);
      return sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
        icon: "success",
        title: "เรียบร้อย",
        text: "อนุมัติการยกเลิก สำเร็จ"
      });
    }, error => {
      console.log(error);
    }); // console.log(seleteddata.total);

    if (seleteddata.type === "money") {
      var messages = "การยกเลิกออม จำนวน " + new Intl.NumberFormat().format(seleteddata.total) + " บาท ของท่านได้รับการอนุมัติแล้ว";
    } else {
      var messages = "การยกเลิกออม จำนวน " + parseFloat(seleteddata.total).toFixed(2) + " กรัม" + "ของท่านได้รับการอนุมัติแล้ว";
    }

    var data = {
      messages: messages,
      user_id: e.user_id
    };
    await axios__WEBPACK_IMPORTED_MODULE_5___default()({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar"
      },
      url: "/api/line_messaging",
      data: JSON.stringify({
        data: data
      })
    }).then(response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
  };

  const onSelectBank = async e => {
    const {
      value
    } = e.target;
    Bank_name_admin(value);
  };

  return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4___default.a, {
      paragraph: true,
      children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
        className: "card mb-5",
        children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
          className: "card-header",
          children: "\u0E15\u0E23\u0E27\u0E08\u0E2A\u0E2D\u0E1A\u0E23\u0E32\u0E22\u0E01\u0E32\u0E23\u0E22\u0E01\u0E40\u0E25\u0E34\u0E01\u0E01\u0E32\u0E23\u0E2D\u0E2D\u0E21"
        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
          className: "card-body",
          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(mdbreact__WEBPACK_IMPORTED_MODULE_3__["MDBDataTableV5"], {
            hover: true,
            entriesOptions: [5, 20, 25],
            entries: 5,
            pagesAmount: 4,
            data: datatable,
            pagingTop: true,
            searchTop: true,
            searchBottom: false
          })
        })]
      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8___default.a, {
        size: "xl",
        show: show,
        dialogClassName: "modal-90w",
        "aria-labelledby": "example-custom-modal-styling-title",
        centered: true,
        style: {
          fontFamily: "SukhumvitSet-SemiBold"
        },
        children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8___default.a.Header, {
          closeButton: true,
          onClick: () => OnCloseDetail(),
          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8___default.a.Title, {
            id: "example-custom-modal-styling-title",
            children: "\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25\u0E01\u0E32\u0E23\u0E22\u0E01\u0E40\u0E25\u0E34\u0E01"
          })
        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8___default.a.Body, {
          style: {
            minHeight: "70vh",
            maxHeight: "70vh",
            overflowY: "auto"
          },
          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("form", {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
              className: "row",
              children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                className: "col-6",
                children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E0A\u0E37\u0E48\u0E2D-\u0E19\u0E32\u0E21\u0E2A\u0E01\u0E38\u0E25:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: flname
                  })]
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E1B\u0E23\u0E30\u0E40\u0E20\u0E17:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: t_name
                  })]
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E08\u0E33\u0E19\u0E27\u0E19:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: totalvalue
                  })]
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E27\u0E31\u0E19\u0E17\u0E35\u0E48:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: moment__WEBPACK_IMPORTED_MODULE_6___default()(seleteddata.cancel_date).format("DD-MM-YYYY")
                  })]
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E0A\u0E37\u0E48\u0E2D\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: seleteddata.bank_name
                  })]
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E40\u0E25\u0E02\u0E17\u0E35\u0E48\u0E1A\u0E31\u0E0D\u0E0A\u0E35:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: seleteddata.bank_number
                  })]
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                  className: "row mb-3",
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-4",
                    children: "\u0E0A\u0E37\u0E48\u0E2D\u0E40\u0E08\u0E49\u0E32\u0E02\u0E2D\u0E07\u0E1A\u0E31\u0E0D\u0E0A\u0E35:"
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                    className: "col-8",
                    children: seleteddata.bank_account
                  })]
                })]
              }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                className: "col-6",
                children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                  className: "card ",
                  children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                    className: "card-body",
                    children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("h5", {
                      className: "card-title",
                      children: "\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25\u0E01\u0E32\u0E23\u0E42\u0E2D\u0E19(\u0E42\u0E2D\u0E19\u0E08\u0E32\u0E01)"
                    }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", {
                      className: "container",
                      children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row ",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                          children: "\u0E0A\u0E37\u0E48\u0E2D\u0E1A\u0E31\u0E0D\u0E0A\u0E35:"
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("input", {
                          className: "custom_date",
                          type: "text",
                          value: bank_account_admin,
                          onChange: e => Bank_account_admin(e.target.value)
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                          children: "\u0E0A\u0E37\u0E48\u0E2D\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23:"
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("select", {
                          contentEditable: true,
                          className: "custom_date",
                          onChange: e => onSelectBank(e),
                          value: bank_name_admin,
                          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            selected: true,
                            disabled: true,
                            children: "\u0E01\u0E23\u0E38\u0E13\u0E32\u0E40\u0E25\u0E37\u0E2D\u0E01\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23..."
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E41\u0E2B\u0E48\u0E07\u0E1B\u0E23\u0E30\u0E40\u0E17\u0E28\u0E44\u0E17\u0E22"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E01\u0E2A\u0E34\u0E01\u0E23\u0E44\u0E17\u0E22"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E01\u0E23\u0E38\u0E07\u0E40\u0E17\u0E1E"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E01\u0E23\u0E38\u0E07\u0E44\u0E17\u0E22"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E01\u0E23\u0E38\u0E07\u0E28\u0E23\u0E35\u0E2D\u0E22\u0E38\u0E18\u0E22\u0E32"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E0B\u0E35\u0E44\u0E2D\u0E40\u0E2D\u0E47\u0E21\u0E1A\u0E35\u0E44\u0E17\u0E22"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E17\u0E2B\u0E32\u0E23\u0E44\u0E17\u0E22"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E44\u0E17\u0E22\u0E1E\u0E32\u0E13\u0E34\u0E0A\u0E22\u0E4C"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E22\u0E39\u0E42\u0E2D\u0E1A\u0E35"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E41\u0E25\u0E19\u0E14\u0E4C\u0E41\u0E2D\u0E19\u0E14\u0E4C \u0E40\u0E2E\u0E49\u0E32\u0E2A\u0E4C"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E2A\u0E41\u0E15\u0E19\u0E14\u0E32\u0E23\u0E4C\u0E14"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E18\u0E19\u0E0A\u0E32\u0E15"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E2D\u0E2D\u0E21\u0E2A\u0E34\u0E19"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E40\u0E01\u0E35\u0E22\u0E23\u0E15\u0E34\u0E19\u0E32\u0E04\u0E34\u0E19\u0E20\u0E31\u0E17\u0E23"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E0B\u0E34\u0E15\u0E35\u0E49\u0E41\u0E1A\u0E07\u0E04\u0E4C"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E2D\u0E32\u0E04\u0E32\u0E23\u0E2A\u0E07\u0E40\u0E04\u0E23\u0E32\u0E30\u0E2B\u0E4C"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23 \u0E18.\u0E01.\u0E2A"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E21\u0E34\u0E0B\u0E39\u0E42\u0E2E"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E2D\u0E34\u0E2A\u0E25\u0E32\u0E21"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E17\u0E34\u0E2A\u0E42\u0E01\u0E49"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E44\u0E2D\u0E0B\u0E35\u0E1A\u0E35\u0E0B\u0E35"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E44\u0E17\u0E22\u0E40\u0E04\u0E23\u0E14\u0E34\u0E15"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E0B\u0E39\u0E21\u0E34\u0E42\u0E15\u0E42\u0E21\u0E21\u0E34\u0E15\u0E0B\u0E38\u0E22"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E40\u0E2D\u0E0A\u0E40\u0E2D\u0E2A\u0E1A\u0E35\u0E0B\u0E35"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E14\u0E2D\u0E22\u0E0B\u0E4C\u0E41\u0E1A\u0E07\u0E01\u0E4C \u0E40\u0E2D\u0E08\u0E35"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E41\u0E2B\u0E48\u0E07\u0E1B\u0E23\u0E30\u0E40\u0E17\u0E28\u0E08\u0E35\u0E19"
                          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("option", {
                            children: "\u0E18\u0E19\u0E32\u0E04\u0E32\u0E23\u0E40\u0E2D\u0E40\u0E2D\u0E47\u0E19\u0E41\u0E0B\u0E14"
                          })]
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row ",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                          children: "\u0E40\u0E25\u0E02\u0E17\u0E35\u0E48\u0E1A\u0E31\u0E0D\u0E0A\u0E35:"
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(react_number_format__WEBPACK_IMPORTED_MODULE_12___default.a, {
                          className: "inputUpload2 ",
                          format: "###-#-#####-###",
                          style: {
                            backgroundColor: "#fff",
                            fontFamily: "SukhumvitSet-SemiBold",
                            color: "rgb(0, 0, 0)",
                            border: " 1px solid #343a40",
                            borderRadius: "0.1rem"
                          },
                          value: bank_number_admin,
                          onValueChange: values => Bank_number_admin(values.value)
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("label", {
                          children: "\u0E27\u0E31\u0E19\u0E40\u0E27\u0E25\u0E32\u0E43\u0E19 \u0E2A\u0E25\u0E34\u0E1B\u0E42\u0E2D\u0E19\u0E40\u0E07\u0E34\u0E19:"
                        })
                      }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                        className: "row",
                        children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                          className: "custom_date",
                          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(react_datepicker__WEBPACK_IMPORTED_MODULE_10___default.a, {
                            selected: transfer_datetime,
                            onChange: e => Transfer_datetime(e),
                            timeInputLabel: "Time:",
                            dateFormat: "MM/dd/yyyy h:mm aa",
                            showTimeInput: true
                          })
                        })
                      })]
                    })]
                  })
                })
              })]
            })
          })
        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_8___default.a.Footer, {
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("button", {
            onClick: () => OnDontApprove(seleteddata),
            type: "button",
            className: "btn btn-danger",
            children: "\u0E44\u0E21\u0E48\u0E2D\u0E19\u0E38\u0E21\u0E31\u0E15\u0E34"
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("button", {
            onClick: () => OnApprove(seleteddata),
            type: "button",
            className: "btn btn-success",
            children: "\u0E2D\u0E19\u0E38\u0E21\u0E31\u0E15\u0E34"
          })]
        })]
      })]
    })
  });
}; //   <span onClick={OnLogout}>Go To login</span> */}


const getServerSideProps = async ({
  req,
  res
}) => {
  const cookies = new cookies__WEBPACK_IMPORTED_MODULE_2___default.a(req, res);
  const user = await cookies.get("user");
  console.log(user);

  if (!user) {
    res.statusCode = 404;
    res.end();
    return {
      props: {}
    };
  }

  return {
    props: {
      user
    }
  };
};
/* harmony default export */ __webpack_exports__["default"] = (PromotionPage);

/***/ }),

/***/ "F5FC":
/***/ (function(module, exports) {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ "SibU":
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Dropdown");

/***/ }),

/***/ "UVoM":
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Typography");

/***/ }),

/***/ "Vjj2":
/***/ (function(module, exports) {

module.exports = require("cookies");

/***/ }),

/***/ "Y52P":
/***/ (function(module, exports) {

module.exports = require("mdbreact");

/***/ }),

/***/ "ZTWx":
/***/ (function(module, exports) {

module.exports = require("react-datepicker");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "qqGZ":
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Modal");

/***/ }),

/***/ "uM63":
/***/ (function(module, exports) {

module.exports = require("react-number-format");

/***/ }),

/***/ "wy2R":
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });