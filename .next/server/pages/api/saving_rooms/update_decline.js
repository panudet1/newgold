module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 67);
/******/ })
/************************************************************************/
/******/ ({

/***/ "1dgg":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const hisSavingRoomsSchema = new mongoose.Schema({
  room_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  token: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  owner: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  weight: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  price: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  start_date: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  end_date: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  day: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  installment: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  member: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  created_date: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  approve_by: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  modify_by: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.hisSavingRooms || mongoose.model("hisSavingRooms", hisSavingRoomsSchema);

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("fzLA");


/***/ }),

/***/ "EkPP":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const SavingRoomsSchema = new mongoose.Schema({
  room_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  token: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  owner: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  weight: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  price: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  start_date: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  end_date: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  day: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  fee: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  installment: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  member: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  created_date: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  approve_by: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.SavingRooms || mongoose.model("SavingRooms", SavingRoomsSchema);

/***/ }),

/***/ "FiKB":
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "RuLO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("FiKB");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.set("useCreateIndex", true);
const connection = {};

async function dbConnect() {
  if (connection.isConnected) {
    return;
  }

  const db = await mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.connect("mongodb+srv://robert:robertmongo@cluster0.60vip.mongodb.net/Gold_saving?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  connection.isConnected = db.connections[0].readyState;
  console.log("isConnected");
}

/* harmony default export */ __webpack_exports__["a"] = (dbConnect);

/***/ }),

/***/ "fzLA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("RuLO");
/* harmony import */ var _models_SavingRooms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("EkPP");
/* harmony import */ var _models_SavingRooms__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_models_SavingRooms__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _models_history_hisSavingRooms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("1dgg");
/* harmony import */ var _models_history_hisSavingRooms__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_models_history_hisSavingRooms__WEBPACK_IMPORTED_MODULE_2__);



Object(_utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])();
/* harmony default export */ __webpack_exports__["default"] = (async (req, res) => {
  const {
    method
  } = req;

  switch (method) {
    case "POST":
      try {
        const filter = {
          _id: req.body.data.id
        };
        const update = {
          status: "declined"
        };
        let doc = await _models_SavingRooms__WEBPACK_IMPORTED_MODULE_1___default.a.findOneAndUpdate(filter, update);
        var item = {
          room_id: doc.room_id,
          token: doc.token,
          owner: doc.owner,
          weight: doc.weight,
          price: doc.price,
          status: doc.status,
          start_date: doc.start_date,
          end_date: doc.end_date,
          day: doc.day,
          fee: doc.fee,
          installment: doc.installment,
          member: doc.member,
          status: "declined",
          approve_by: req.body.data.user_id,
          created_date: new Date(),
          modify_by: req.body.data.user_id
        };
        await _models_history_hisSavingRooms__WEBPACK_IMPORTED_MODULE_2___default.a.create(item);
        res.status(200).json({
          success: true,
          data: doc
        });
      } catch (error) {
        res.status(400).json({
          success: false
        });
      }

      break;

    default:
      res.status(400).json({
        success: false
      });
      break;
  }
});

/***/ })

/******/ });