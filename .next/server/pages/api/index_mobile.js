module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 25);
/******/ })
/************************************************************************/
/******/ ({

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("qfwS");


/***/ }),

/***/ "FiKB":
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "GL94":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const RegisterSchema = new mongoose.Schema({
  fname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  lname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  phonenumber: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  idcard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  brithday: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  address: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  province: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  district: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  canton: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  postal_code: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  user_line_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  fileUserWithIdCard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  dateRegister: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [100, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.Register || mongoose.model("Register", RegisterSchema);

/***/ }),

/***/ "OiCc":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const UserSchema = new mongoose.Schema({
  user_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  password: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  role: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  fname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  lname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  phonenumber: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  idcard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  brithday: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  address: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  province: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  district: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  canton: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  postal_code: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  user_line_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  fileIdCard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_name: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_number: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_account: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  point: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  dateCreate: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  createby: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.User || mongoose.model("User", UserSchema);

/***/ }),

/***/ "RuLO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("FiKB");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.set("useCreateIndex", true);
const connection = {};

async function dbConnect() {
  if (connection.isConnected) {
    return;
  }

  const db = await mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.connect("mongodb+srv://robert:robertmongo@cluster0.60vip.mongodb.net/Gold_saving?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  connection.isConnected = db.connections[0].readyState;
  console.log("isConnected");
}

/* harmony default export */ __webpack_exports__["a"] = (dbConnect);

/***/ }),

/***/ "Vjj2":
/***/ (function(module, exports) {

module.exports = require("cookies");

/***/ }),

/***/ "qfwS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("RuLO");
/* harmony import */ var _models_User__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("OiCc");
/* harmony import */ var _models_User__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_models_User__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _models_Register__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("GL94");
/* harmony import */ var _models_Register__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_models_Register__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var cookies__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("Vjj2");
/* harmony import */ var cookies__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(cookies__WEBPACK_IMPORTED_MODULE_3__);



Object(_utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])();

/* harmony default export */ __webpack_exports__["default"] = (async (req, res) => {
  if (req.method === "POST") {
    const {
      id
    } = req.body;
    const user_data = await _models_User__WEBPACK_IMPORTED_MODULE_1___default.a.find({
      user_id: id
    });
    const register_data = await _models_Register__WEBPACK_IMPORTED_MODULE_2___default.a.find({
      user_line_id: id
    });

    if (user_data[0] == undefined) {
      if (register_data[0] == undefined) {
        return res.status(202).json({
          //ไม่มีข้อมูลในระบบ
          status: "nodata"
        });
      } else {
        if (register_data[0].status == "pending") {
          //มีข้อมูลในระบบ สมัครสมาชิก รอการยืนยัน
          return res.status(201).json({
            status: "pending"
          });
        } else {
          return res.status(202).json({
            //ไม่มีข้อมูลในระบบ
            status: "nodata"
          });
        }
      }
    } else {
      if (user_data[0].status == "nopass") {
        return res.status(203).json({
          //ยืนยันรหัสผ่าน
          status: "fristime"
        });
      } else {
        return res.status(200).json({
          //กรอกรหัสเข้าใช้งาน
          status: "success"
        });
      }
    }
  }
});

/***/ })

/******/ });