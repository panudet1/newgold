module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 88);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+vFl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("RuLO");
/* harmony import */ var _models_Withdraw__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("4pP6");
/* harmony import */ var _models_Withdraw__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_models_Withdraw__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _models_history_hisWithdraw__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("WVqB");
/* harmony import */ var _models_history_hisWithdraw__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_models_history_hisWithdraw__WEBPACK_IMPORTED_MODULE_2__);



Object(_utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])();
/* harmony default export */ __webpack_exports__["default"] = (async (req, res) => {
  const {
    method
  } = req;

  switch (method) {
    case "POST":
      try {
        // console.log(req.body.data);
        var item = {
          user_id: req.body.data.user_id,
          room_id: req.body.data.room_id,
          type: req.body.data.type,
          total: req.body.data.total,
          bank_name: req.body.data.bank_name,
          bank_number: req.body.data.bank_number,
          bank_account: req.body.data.bank_account,
          change: req.body.data.change,
          //เงินทอน
          point: req.body.data.point,
          //เงินทอน
          bank_name_admin: "",
          bank_number_admin: "",
          bank_account_admin: "",
          transfer_datetime: "",
          withdraw_date: new Date(),
          status: "pending"
        };
        const withdraw = await _models_Withdraw__WEBPACK_IMPORTED_MODULE_1___default.a.create(item);
        var hisitem = {
          user_id: req.body.data.user_id,
          room_id: req.body.data.room_id,
          type: req.body.data.type,
          total: req.body.data.total,
          bank_name: req.body.data.bank_name,
          bank_number: req.body.data.bank_number,
          bank_account: req.body.data.bank_account,
          bank_name_admin: "",
          bank_number_admin: "",
          bank_account_admin: "",
          transfer_datetime: "",
          change: req.body.data.change,
          //เงินทอน
          point: req.body.data.point,
          //เงินทอน
          withdraw_date: new Date(),
          status: "pending",
          modify_by: req.body.data.user_id
        };
        await _models_history_hisWithdraw__WEBPACK_IMPORTED_MODULE_2___default.a.create(hisitem);
        res.status(200).json({
          success: true,
          data: withdraw
        });
      } catch (error) {
        res.status(400).json({
          success: false
        });
      }

      break;

    case "GET":
      break;

    default:
      res.status(400).json({
        success: false
      });
      break;
  }
});

/***/ }),

/***/ "4pP6":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const WithdrawSchema = new mongoose.Schema({
  user_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  room_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  type: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  total: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_name: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_number: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_account: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_name_admin: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_number_admin: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_account_admin: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  transfer_datetime: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  change: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  point: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  withdraw_date: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.WithdrawSchema || mongoose.model("Withdraw", WithdrawSchema);

/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("+vFl");


/***/ }),

/***/ "FiKB":
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "RuLO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("FiKB");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.set("useCreateIndex", true);
const connection = {};

async function dbConnect() {
  if (connection.isConnected) {
    return;
  }

  const db = await mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.connect("mongodb+srv://robert:robertmongo@cluster0.60vip.mongodb.net/Gold_saving?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  connection.isConnected = db.connections[0].readyState;
  console.log("isConnected");
}

/* harmony default export */ __webpack_exports__["a"] = (dbConnect);

/***/ }),

/***/ "WVqB":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const hisWithdrawSchema = new mongoose.Schema({
  user_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  room_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  type: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  total: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_name: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_number: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_account: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_name_admin: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_number_admin: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  bank_account_admin: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  transfer_datetime: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  change: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  point: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  withdraw_date: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  modify_by: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.hisWithdrawSchema || mongoose.model("hisWithdraw", hisWithdrawSchema);

/***/ })

/******/ });