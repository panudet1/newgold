module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 46);
/******/ })
/************************************************************************/
/******/ ({

/***/ 46:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("jgy0");


/***/ }),

/***/ "4T1P":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const hisRegisterSchema = new mongoose.Schema({
  fname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  lname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  phonenumber: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  idcard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  brithday: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  address: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  province: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  district: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  canton: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  postal_code: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  user_line_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  dateRegister: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [100, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.hisRegister || mongoose.model("hisRegister", hisRegisterSchema);

/***/ }),

/***/ "FiKB":
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "GL94":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const RegisterSchema = new mongoose.Schema({
  fname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  lname: {
    type: String,
    required: [true, "Please add a title"],
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  phonenumber: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  idcard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  brithday: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  address: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  province: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  district: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  canton: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  postal_code: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  user_line_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  fileUserWithIdCard: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  dateRegister: {
    type: Date,
    maxlength: [300, "Title cannot be more than 40 characters"]
  },
  status: {
    type: String,
    maxlength: [100, "Title cannot be more than 40 characters"]
  }
});
module.exports = mongoose.model.Register || mongoose.model("Register", RegisterSchema);

/***/ }),

/***/ "RuLO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("FiKB");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.set("useCreateIndex", true);
const connection = {};

async function dbConnect() {
  if (connection.isConnected) {
    return;
  }

  const db = await mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.connect("mongodb+srv://robert:robertmongo@cluster0.60vip.mongodb.net/Gold_saving?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  connection.isConnected = db.connections[0].readyState;
  console.log("isConnected");
}

/* harmony default export */ __webpack_exports__["a"] = (dbConnect);

/***/ }),

/***/ "jgy0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("RuLO");
/* harmony import */ var _models_Register__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("GL94");
/* harmony import */ var _models_Register__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_models_Register__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _models_history_hisRegister__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("4T1P");
/* harmony import */ var _models_history_hisRegister__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_models_history_hisRegister__WEBPACK_IMPORTED_MODULE_2__);



Object(_utils_dbConnect__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])();
/* harmony default export */ __webpack_exports__["default"] = (async (req, res) => {
  const {
    method
  } = req;

  switch (method) {
    case "POST":
      try {
        let hisitem = {
          fname: req.body.data.fname,
          lname: req.body.data.lname,
          phonenumber: req.body.data.phonenumber,
          idcard: req.body.data.idcard,
          brithday: req.body.data.brithday,
          expiry: req.body.data.expiry,
          backidcard: req.body.data.backidcard,
          job: req.body.data.job,
          job_location: req.body.data.job_location,
          address: req.body.data.address,
          province: req.body.data.province,
          district: req.body.data.district,
          canton: req.body.data.canton,
          postal_code: req.body.data.postal_code,
          user_line_id: req.body.data.user_line_id,
          status: req.body.data.status,
          dateRegister: new Date()
        };
        const hisregister = await _models_history_hisRegister__WEBPACK_IMPORTED_MODULE_2___default.a.create(hisitem);
        let item = {
          fname: req.body.data.fname,
          lname: req.body.data.lname,
          phonenumber: req.body.data.phonenumber,
          idcard: req.body.data.idcard,
          brithday: req.body.data.brithday,
          expiry: req.body.data.expiry,
          backidcard: req.body.data.backidcard,
          job: req.body.data.job,
          job_location: req.body.data.job_location,
          address: req.body.data.address,
          province: req.body.data.province,
          district: req.body.data.district,
          canton: req.body.data.canton,
          postal_code: req.body.data.postal_code,
          user_line_id: req.body.data.user_line_id,
          fileIdCard: req.body.data.fileIdCard,
          fileUserWithIdCard: req.body.data.fileUserWithIdCard,
          status: req.body.data.status,
          dateRegister: new Date()
        };
        const register = await _models_Register__WEBPACK_IMPORTED_MODULE_1___default.a.create(item);
        res.status(200).json({
          success: true,
          data: register
        });
      } catch (error) {
        res.status(400).json({
          success: false
        });
      }

      break;

    case "GET":
      try {
        const register = await _models_Register__WEBPACK_IMPORTED_MODULE_1___default.a.find({
          status: "pending"
        }); // console.log(register);

        res.status(200).json({
          success: true,
          data: register
        });
      } catch (error) {
        res.status(400).json({
          success: false
        });
      }

      break;

    default:
      res.status(400).json({
        success: false
      });
      break;
  }
});

/***/ })

/******/ });